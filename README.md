# Building this project

Here are the steps you'll need to follow to get this project up and running:

## Prerequisites
This app is built using Rails 5 and Ember. To ensure you can run the app, here are a few requirements:
1. Make sure you have Ruby installed and that your version is up to date (this was built using *2.4.1*)
1. Make sure you have the *bundler* gem installed for your Ruby version
1. Make sure you have *node* and *npm* installed and that both are up to date

## Rails API
1. Clone this repo: `git clone git@gitlab.com:nategraves/bonobos.git`
1. Change directories into the project: `cd bonobos`
1. Run `bundle install` to install needed gems
1. Run migrations: `rake db:migrate` to create the sqlite db and its tables
1. Seed the DB with our test data: `rake db:seed`
1. Start the rails server: `rails s`

## Ember Frontend
1. In a new tab or window, change into the frontend directory of the project: `cd frontend`
1. Install dependencies: `npm install`
1. Start serving the frontend `ember server --proxy http://localhost:3000`
1. The project should now be available at [http:localhost:4200](http:localhost:4200)

# My Approach

For this project, I decided to use Rails 5 and Ember because I'm pretty familiar with both. I hadn't, however, had a chance to work much with Rails 5's new API mode or with Ember's relatively recent JSON-API support. Rails and Ember play nicely together and this project was definitely sped up by being able to make use of both Rails's and Ember's generators. Ember's new JSON API Serializer made it easy to fulfill the requirement of grouping inventory by prodct.

# Future Improvements
While building this, there were a few areas I noticed could be improved. Currently, I'm loading all products and their nested inventory entries when you visit the page. This isn't ideal as any additional growth to the tables will further slow the request. This could be improved in a couple ways. First, you could paginate the request to get products. This would help speed up the query and reduce the data payload size. Second, the inventory could be loaded asynchronously so that you're not waiting for that additional data before rendering the bulk of the page. Also, the products could be cached to further improve subsequent page load times. There are also some database improvements that could be made. You definitely wouldn't want to use a sqlite database in production, so switching to something like MySQL or PostgreSQL would offer performance improvements (especially with a larger dataset). And finally, adding an index to the inventory would speed further speed up product-specific queries.
