class Product < ApplicationRecord
  has_many :inventory, dependent: :destroy
  validates_presence_of :product_name, :product_image, :product_description
end

