class ProductSerializer < ActiveModel::Serializer
  attributes :id, :product_name, :product_image, :product_description, :product_id
  has_many :inventory, foreign_key: :product_id
end
