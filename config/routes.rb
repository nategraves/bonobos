Rails.application.routes.draw do
  resources :inventories
  resources :products
end
