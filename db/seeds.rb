require 'csv'

CSV.foreach('products.csv', headers: true) do |row|
  Product.create!(row.to_hash)
end

CSV.foreach('inventory.csv', headers: true) do |row|
  Inventory.create!(row.to_hash)
end

