class CreateInventories < ActiveRecord::Migration[5.0]
  def change
    create_table :inventories do |t|
      t.integer :waist
      t.integer :length
      t.string :style
      t.integer :count
      t.references :product

      t.timestamps
    end
  end
end
