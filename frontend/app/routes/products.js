import Ember from 'ember';

export default Ember.Route.extend({
  model() {
    return Ember.RSVP.hash({
      products: this.store.findAll('product', { include: 'inventories' }),
      inventories: this.store.findAll('inventory')
    });
  }
});
