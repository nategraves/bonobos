import DS from 'ember-data';

export default DS.Model.extend({
  waist: DS.attr('number'),
  length: DS.attr('number'),
  style: DS.attr('string'),
  count: DS.attr('number'),
  product: DS.belongsTo('product')
});
