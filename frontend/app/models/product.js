import DS from 'ember-data';

export default DS.Model.extend({
  productName: DS.attr('string'),
  productImage: DS.attr('string'),
  productDescription: DS.attr('string'),
  inventories: DS.hasMany('inventory')
});
