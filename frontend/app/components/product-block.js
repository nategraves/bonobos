import Ember from 'ember';

export default Ember.Component.extend({
  product: null,
  inventories: Ember.computed.alias('product.inventories'),

  columns: [
    { 'propertyName': 'style', 'title': 'Style', 'filterWithSelect': true },
    { 'propertyName': 'waist', 'title': 'Waist Size', 'filterWithSelect': true },
    { 'propertyName': 'length', 'title': 'Inseam', 'filterWithSelect': true },
    { 'propertyName': 'count', 'title': '# In Stock', 'disableFiltering': true }
  ],

  customIcons: Ember.Object.create({
    "sort-asc": "fa fa-caret-down",
    "sort-desc": "fa fa-caret-up",
    "nav-first": "fa fa-angle-double-left",
    "nav-prev": "fa fa-angle-left",
    "nav-next": "fa fa-angle-right",
    "nav-last": "fa fa-angle-double-right",
    "expand-row": "fa fa-plus",
    "collapse-row": "fa fa-minus"
  }),

  customClasses: Ember.Object.create({
    "tfooterWrapper": "table-footer row",
    "footerSummaryDefaultPagination": "col-md-7 col-sm-7 col-xs-7"
  })
});
