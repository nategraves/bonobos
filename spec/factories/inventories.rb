FactoryGirl.define do
  factory :inventory do
    waist { Faker::Number.between(28, 36) }
    length { Faker::Number.between(30, 38) }
    style { Faker::Commerce.color }
    count { Faker::Number.between(0, 100) }
    product
  end
end
