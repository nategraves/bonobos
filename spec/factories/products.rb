FactoryGirl.define do
  factory :product do
    product_name { Faker::Commerce.product_name }
    product_image { Faker::Internet.url('bonobos-prod-s3.imgix.net/products/25887/original/t92qwcahevxivaob92jkm4x9pbsw2yz7.jpg') }
    product_description { Faker::Hipster.paragraph }
  end
end
